#!/bin/bash
echo "Criando secrets no cluster kubernetes..."

kubectl apply -f ./secrets.yml

echo "Criando servicos no cluster kubernetes..."

kubectl apply -f ./service.yml

echo "Criando os deployments..."

kubectl apply -f ./deployment.yml

echo "Deploy da aplicacao finalizado"